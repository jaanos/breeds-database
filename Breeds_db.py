__author__ = 'Lena'
import auth
import psycopg2

baza = psycopg2.connect(database=auth.db, host=auth.host, user=auth.user, password=auth.password)
c = baza.cursor()


c.execute('''CREATE TABLE IF NOT EXISTS breeder (
  breeder_id SERIAL,
  first_name VARCHAR(1000) NOT NULL,
  last_name VARCHAR(1000) NOT NULL,
  contact VARCHAR(1000) NOT NULL,
  city VARCHAR(1000) NOT NULL,
  latitude NUMERIC NOT NULL,
  longitude NUMERIC NOT NULL,
  country VARCHAR(1000) NOT NULL,
  PRIMARY KEY (breeder_id)
  )''')

c.execute('''CREATE TABLE IF NOT EXISTS breed (
  breed_id SERIAL,
  breed_name VARCHAR(1000) NOT NULL UNIQUE,
  life_span_min REAL,
  life_span_max REAL,
  life_span_average REAL,
  puppy_price_min INTEGER,
  puppy_price_max INTEGER,
  puppy_price_average INTEGER,
  good_with_kids SMALLINT,
  cat_friendly SMALLINT,
  dog_friendly SMALLINT,
  trainability SMALLINT,
  shedding SMALLINT,
  watchdog SMALLINT,
  intelligence SMALLINT,
  grooming SMALLINT,
  popularity SMALLINT,
  adaptability SMALLINT,
  hypoallergenic BOOLEAN,
  overview TEXT,
  body_type TEXT,
  coat TEXT,
  color_ex TEXT,
  temperament_ex TEXT,
  more_info TEXT,
  wikipedia TEXT,
  photo TEXT,
  poorly_suited_for_hot_weather BOOLEAN,
  well_suited_for_hot_weather BOOLEAN,
  well_suited_for_cold_weather BOOLEAN,
  poorly_suited_for_apartment BOOLEAN,
  well_suited_for_apartment BOOLEAN,
  high_energy BOOLEAN,
  less_exercise BOOLEAN,
  lots_of_exercise BOOLEAN,
  PRIMARY KEY (breed_id)
  )''')

c.execute('''CREATE TABLE IF NOT EXISTS litter (
  litter_id SERIAL,
  breed_id INTEGER,
  breeder_id INTEGER,
  litter_size SMALLINT NOT NULL CHECK (litter_size > 0),
  PRIMARY KEY (litter_id),
  FOREIGN KEY (breed_id) REFERENCES breed(breed_id),
  FOREIGN KEY (breeder_id) REFERENCES breeder(breeder_id)
  )''')


c.execute('''CREATE TABLE IF NOT EXISTS breed_size (
  size_type_id SERIAL,
  size_type VARCHAR(10) UNIQUE,
  PRIMARY KEY (size_type_id)
  )''')

c.execute('''CREATE TABLE IF NOT EXISTS breed_group(
  breed_group_id SERIAL,
  breed_group VARCHAR(1000) UNIQUE,
  PRIMARY KEY (breed_group_id)
  )''')

c.execute('''CREATE TABLE IF NOT EXISTS breed_group_breed (
  breed_id INTEGER REFERENCES breed(breed_id) ON DELETE CASCADE,
  breed_group_id INTEGER REFERENCES breed_group(breed_group_id) ON DELETE CASCADE,
  PRIMARY KEY (breed_id, breed_group_id)
  )''')

c.execute('''CREATE TABLE IF NOT EXISTS breed_size_breed (
  breed_id INTEGER REFERENCES breed(breed_id) ON DELETE CASCADE,
  size_type_id INTEGER REFERENCES breed_size(size_type_id) ON DELETE CASCADE,
  PRIMARY KEY (breed_id, size_type_id)
  )''')

c.execute('''CREATE TABLE IF NOT EXISTS other_breed_name (
  other_name_id SERIAL,
  breed_id INTEGER,
  other_name VARCHAR(1000),
  FOREIGN KEY (breed_id) REFERENCES breed(breed_id)
  )''')

c.execute('''CREATE TABLE IF NOT EXISTS origin(
  origin_id SERIAL,
  origin VARCHAR(1000),
  PRIMARY KEY (origin_id)
  )''')

c.execute('''CREATE TABLE IF NOT EXISTS temperament(
  temperament_id SERIAL,
  temperament VARCHAR(1000),
  PRIMARY KEY (temperament_id)
  )''')

c.execute('''CREATE TABLE IF NOT EXISTS color(
  color_id SERIAL,
  color VARCHAR(1000),
  PRIMARY KEY (color_id)
  )''')

c.execute('''CREATE TABLE IF NOT EXISTS origin_breed (
  breed_id SERIAL REFERENCES breed ON DELETE CASCADE,
  origin_id SERIAL REFERENCES origin ON DELETE CASCADE,
  PRIMARY KEY (breed_id, origin_id)
  )''')

c.execute('''CREATE TABLE IF NOT EXISTS color_breed (
  breed_id SERIAL REFERENCES breed ON DELETE CASCADE,
  color_id SERIAL REFERENCES color ON DELETE CASCADE,
  PRIMARY KEY (breed_id, color_id)
  )''')

c.execute('''CREATE TABLE IF NOT EXISTS temperament_breed (
  breed_id SERIAL REFERENCES breed ON DELETE CASCADE,
  temperament_id SERIAL REFERENCES temperament ON DELETE CASCADE,
  PRIMARY KEY (breed_id, temperament_id)
  )''')

c.execute('''CREATE TABLE IF NOT EXISTS height_weight (
  breed_id INTEGER,
  size_type VARCHAR(10) REFERENCES breed_size(size_type),
  sex CHAR(1) CHECK (sex = 'M' OR sex = 'F'),
  min_height REAL,
  max_height REAL,
  average_height REAL,
  min_weight REAL,
  max_weight REAL,
  average_weight REAL,
  PRIMARY KEY (breed_id,sex,size_type),
  FOREIGN KEY (breed_id) REFERENCES breed(breed_id)
  )''')

baza.commit()

